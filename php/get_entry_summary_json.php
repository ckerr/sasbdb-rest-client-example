<?php

include_once("base.php");

$SASBDB_code = 'SASDAD7';
$url= API_BASE.'entry/summary/'.$SASBDB_code;

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

//set headers
$headers = array();
$headers[] = 'Accept: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);

$result = curl_exec($ch);
curl_close ($ch);

if($result !== false) {
    $data = json_decode($result);
    if(!isset($data->error)){
        echo $data->code . PHP_EOL ;
        echo $data->experiment->sample->name . PHP_EOL;
        echo $data->status . PHP_EOL;
    }else{
        echo "There is an error in the request.";
    }
}


?>
