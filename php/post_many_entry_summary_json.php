<?php

include_once("base.php");

$SASBDB_codes = 'SASDAD7,SASDAA9,SASDAX8';
$url= API_BASE.'entry/summary/list/';

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

//Passing the SASBDB codes in the POST request
curl_setopt($ch, CURLOPT_POSTFIELDS,"codes=$SASBDB_codes");

//set headers
$headers = array();
$headers[] = 'Accept: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);

$result = curl_exec($ch);
curl_close ($ch);

if($result !== false) {
    $data = json_decode($result);
    if(!isset($data->error)){
         foreach($data as $object) {
            echo $object->code . PHP_EOL ;
            echo $object->experiment->sample->name . PHP_EOL;
            echo $object->status . PHP_EOL;
        }
    }else{
        echo "There is an error in the request.";
    }
}


?>
