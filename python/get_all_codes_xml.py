#Requests library is required - http://docs.python-requests.org/
import requests
from base import API_BASE
import xml.etree.ElementTree as ET

try:
    resp = requests.get(API_BASE + 'entry/codes/all', headers={"Accept":"application/xml"} )
    #It is possible to indicate the format also direclty in the URL
    #resp = requests.get(API_BASE + 'entry/codes/all.xml')
    if resp.status_code != 200:
        resp.raise_for_status()
    else:
        tree = ET.fromstring(resp.content)
        for entry in tree.findall('entry'):
            print entry.find('code').text + " " + entry.find('status').text
            
except requests.exceptions.HTTPError:
    resp.raise_for_status()