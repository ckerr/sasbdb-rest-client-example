#Requests library is required - http://docs.python-requests.org/
import requests
from base import API_BASE

SASBDB_code = 'SASDAH7'

# Instead of adding the SASBDB code to the path, it can be
# submitted as a query string ("?code=SASDAH7")
# This is done using the 'params=' keyword argument 
resp = requests.get(API_BASE + 'entry/summary/',
                    params={'code': SASBDB_code},
                    headers={"Accept": "application/json"})

if resp.status_code != 200:
    resp.raise_for_status()
else:
    json_data =  resp.json() 
    print(json_data['code'])
    print(json_data['status'])
    print(json_data['experiment']['sample']['name'])
