#Requests library is required - http://docs.python-requests.org/
import requests
from base import API_BASE

try:
    resp = requests.get(API_BASE + 'entry/codes/all', headers={"Accept":"application/json"} )
    #It is possible to indicate the format also direclty in the URL
    #resp = requests.get(API_BASE + 'entry/codes/all.json')
    if resp.status_code != 200:
        resp.raise_for_status()
    else:
        for entries in resp.json():
           print('{} {}'.format(entries['code'], entries['status']))
except requests.exceptions.HTTPError:
    resp.raise_for_status()