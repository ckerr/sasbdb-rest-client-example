#Requests library is required - http://docs.python-requests.org/
import requests
from base import API_BASE
import xml.etree.ElementTree as ET

SASBDB_code = 'SASDAD7'

try:
    resp = requests.get(API_BASE + 'entry/summary/'+ SASBDB_code , headers={"Accept":"application/xml"} )
    #It is possible to indicate the format also direclty in the URL
    #resp = requests.get(API_BASE + 'entry/summary/'+ SASBDB_code + ".xml")
    if resp.status_code != 200:
        resp.raise_for_status()
    else:
        tree = ET.fromstring(resp.content)
        print tree.find('code').text
        print tree.find('status').text
        print tree.find('experiment').find('sample').find('name').text
            
except requests.exceptions.HTTPError:
    resp.raise_for_status()
