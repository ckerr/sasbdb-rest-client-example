#Requests library is required - http://docs.python-requests.org/
import requests
from base import API_BASE
import xml.etree.ElementTree as ET

SASBDB_codes = 'SASDAD7,SASDAA9,SASDAX8'
data = {'codes': SASBDB_codes}

try:
    resp = requests.post(API_BASE + 'entry/summary/list/', data, headers={"Accept":"application/json"})
    if resp.status_code != 200:
        resp.raise_for_status()
    else:
        for entry in resp.json():
            print(entry['code'])
            print(entry['experiment']['sample']['name'])
            print(entry['status'])
            
except requests.exceptions.HTTPError:
    resp.raise_for_status()
