#Requests library is required - http://docs.python-requests.org/
import requests
from base import API_BASE

SASBDB_code = 'SASDAD7'

try:
    resp = requests.get(API_BASE + 'entry/summary/'+ SASBDB_code , headers={"Accept":"application/json"} )
    #It is possible to indicate the format also direclty in the URL
    #resp = requests.get(API_BASE + 'entry/summary/' + SASBDB_code +".json")
    if resp.status_code != 200:
        resp.raise_for_status()
    else:
        json_data =  resp.json() 
        print(json_data['code'])
        print(json_data['status'])
        print(json_data['experiment']['sample']['name'])
except requests.exceptions.HTTPError:
    resp.raise_for_status()
